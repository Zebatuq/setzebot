﻿using System;
using System.Data.SQLite;
using System.Threading;
using System.Threading.Tasks;
using SteamAuth;
using SteamKit2;

namespace SetZebot
{
    internal static class Program
    {
        #region Инициализация

        private static SteamClient SteamClient;
        private static CallbackManager Manager;
        private static SteamUser SteamUser;
        public static SteamFriends SteamFriends;
        private static UserLogin UserWebLogin;
        private static bool IsRunning;

        #endregion
        
        #region Подключение

        private static void Main()
        {
            Config.LoadConfig();
            Console.WriteLine(Database.GetUserByID(1231444));
            
            /*
            SQLiteCommand cmd = new SQLiteCommand("");
            SteamClient = new SteamClient();
            Manager = new CallbackManager(SteamClient);
            UserWebLogin = new UserLogin(Config.Login, Config.Password);

            SteamUser = SteamClient.GetHandler<SteamUser>();
            SteamFriends = SteamClient.GetHandler<SteamFriends>();

            Manager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
            Manager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);
            Manager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedOn);
            Manager.Subscribe<SteamUser.LoggedOffCallback>(OnLoggedOff);

            Manager.Subscribe<SteamFriends.FriendMsgCallback>(ChatMsg);

            SteamClient.Connect();

            IsRunning = true;
            while (IsRunning)
            {
                Manager.RunWaitCallbacks(TimeSpan.FromSeconds(1));
            }
            */
            Console.ReadKey();
        }

        private static void WebLogin()
        {
            Console.WriteLine("Веб авторизация..");
            UserWebLogin.TwoFactorCode = Tools.TwoFaCode();
            if (UserWebLogin.DoLogin() != LoginResult.LoginOkay)
            {
                Console.WriteLine("Ошибка Веб-Авторизации: " + UserWebLogin.DoLogin());
                Console.WriteLine("Повторная авторизация через 60 секунд..");
                Thread.Sleep(60 * 1000);
                WebLogin();
                return;
            }

            Console.WriteLine("Успешная Веб авторизация!");
        }

        #endregion
        
        #region Каллбеки

        private static void OnConnected(SteamClient.ConnectedCallback callback)
        {
            Console.WriteLine("Подключен к Steam! Авторизуюсь..");
            SteamUser.LogOn(new SteamUser.LogOnDetails
            {
                Username = Config.Login,
                Password = Config.Password,
                TwoFactorCode = Tools.TwoFaCode()
            });
        }

        private static void OnDisconnected(SteamClient.DisconnectedCallback callback)
        {
            Console.WriteLine("Отключен от стима! Переподключение через 10 секунд..");
            Thread.Sleep(10 * 1000);
            SteamClient.Connect();
        }

        private static void OnLoggedOn(SteamUser.LoggedOnCallback callback)
        {
            if (callback.Result != EResult.OK)
            {
                Console.WriteLine("Ошибка авторизации SteamKit: {0} / {1}", callback.Result, callback.ExtendedResult);
                Console.WriteLine("Повторная авторизация через 30 секунд..");
                Thread.Sleep(30 * 1000);
                SteamUser.LogOn(new SteamUser.LogOnDetails
                {
                    Username = Config.Login,
                    Password = Config.Password,
                    TwoFactorCode = Tools.TwoFaCode()
                });
                return;
            }

            SteamFriends.SetPersonaState(EPersonaState.Online);

            Console.WriteLine("Успешная авторизация SteamKit!");
            Console.WriteLine("Веб авторизация через 30 секунд..");
            Thread.Sleep(30 * 1000);
            WebLogin();
        }

        private static void OnLoggedOff(SteamUser.LoggedOffCallback callback)
        {
            Console.WriteLine("Отключен от стима: " + callback.Result);
            Console.WriteLine("Переподключение через 30 секунд..");
            Thread.Sleep(30 * 1000);

            SteamUser.LogOn(new SteamUser.LogOnDetails
            {
                Username = Config.Login,
                Password = Config.Password,
                TwoFactorCode = Tools.TwoFaCode()
            });
        }

        private static void ChatMsg(SteamFriends.FriendMsgCallback obj)
        {
            if (obj.EntryType == EChatEntryType.ChatMsg)
            {
                Task.Run(() => Commands.NewMessage(obj.Sender, obj.Message));
            }
        }

        #endregion
    }
}