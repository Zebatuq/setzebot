using System.Collections.Generic;
using System.Linq;
using SteamKit2;

namespace SetZebot
{
    public static class Commands
    {
        public static void NewMessage(ulong steamID, string message)
        {
            if (message.StartsWith("!"))
            {
                message = message.Substring(0, 0);
                List<string> command = message.Split(' ').ToList();
                NewCommand(steamID, command);
                return;
            }

            Program.SteamFriends.SendChatMessage(steamID, EChatEntryType.ChatMsg, "!help писщчи");
        }

        private static void NewCommand(ulong steamID, List<string> command)
        {
            switch (command[0])
            {
                case "ping":
                    Program.SteamFriends.SendChatMessage(steamID, EChatEntryType.ChatMsg, "pong!");
                    break;
                default:
                    Program.SteamFriends.SendChatMessage(steamID, EChatEntryType.ChatMsg, "!help писщчи");
                    break;
            }
        }
    }
}