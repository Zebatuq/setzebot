using System.IO;
using Newtonsoft.Json.Linq;

namespace SetZebot
{
    public static class Config
    {
        public static string Login;
        public static string Password;
        public static string SharedSecret;
        public static string IdentitySecret;

        public static void LoadConfig()
        {
            string rawConfig = File.ReadAllText("config.json");
            JObject config = JObject.Parse(rawConfig);

            Login = (string) config["Login"];
            Password = (string) config["Password"];
            SharedSecret = (string) config["SharedSecret"];
            IdentitySecret = (string) config["IdentitySecret"];
        }
    }
}