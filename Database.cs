using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SetZebot
{
    public static class Database
    {
        private const string UsersDB = "./data/users.json";

        private static JArray OpenDB(string nameDB)
        {
            string text = File.ReadAllText(nameDB);
            JArray jArray = JArray.Parse(text);
            return jArray;
        }
        
        private static void saveDB(string nameDB, IEnumerable data)
        {
            File.WriteAllText(nameDB, JsonConvert.SerializeObject(data, Formatting.Indented) );
        }

        public static object GetUserByID(ulong steamID)
        {
            JArray users = OpenDB(UsersDB);
            try
            {
                JToken user = users.First(x => (ulong) x["SteamID"] == steamID);
                return user;
            }
            catch (Exception e)
            {
                if (e.Message == "Sequence contains no matching element")
                {
                    JObject newUser = new JObject
                    {
                        { "SteamID", steamID },
                        { "Lang", "eng" }
                    };
                    users.Add( newUser );
                    saveDB(UsersDB, users);
                    return newUser;
                }
                
                Console.WriteLine(e);
                Environment.Exit(1);
                return null;
            }
        }
    }
}