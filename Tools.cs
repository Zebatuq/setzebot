using System;
using SteamAuth;

namespace SetZebot
{
    public static class Tools
    {
        public static string TwoFaCode()
        {

            return new SteamGuardAccount {SharedSecret = Config.SharedSecret, IdentitySecret = Config.IdentitySecret}.GenerateSteamGuardCode();
        }
    }
}